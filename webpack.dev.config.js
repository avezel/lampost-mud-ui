const webpack = require('webpack');
const config = require('./webpack.config.js');

config.plugins.push(new webpack.DefinePlugin({
  'process.env': {
    NODE_ENV: JSON.stringify('dev')
  }
}));

config.devServer = {
    historyApiFallback: true,
    proxy: {
      '/link': {
        target: 'http://localhost:2500',
        ws: true
      }
    }
  };

module.exports = config;
