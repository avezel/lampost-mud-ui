import bootstrap from 'lampost-ui';
import displayStore from 'lampost-ui/display/store'
import { register } from 'lampost-ui/service/dispatcher';
import { send } from 'lampost-ui/service/remote';

import Mud from './mud/mud.jsx';

displayStore.addPage('/mud', Mud);
displayStore.enablePage('/mud');

register('user_created', (data) => {
  send('player_login', {player_id: data.user.user_name});
});

bootstrap();
