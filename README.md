## lampost-mud-ui

Experimental React UI for Lampost Mud, built on the lampost-ui library.


## License

The code is available under the [MIT license](LICENSE.txt).
