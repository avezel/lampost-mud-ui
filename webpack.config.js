const path = require('path');
const copyPlugin = require('copy-webpack-plugin');
const webpack = require('webpack');

let version = require('./package.json').version;
if (version.indexOf('dev') > -1) {
  version = version + Math.floor(Date.now() / 1000);
}

const lampostSrc = path.resolve(__dirname, './node_modules/lampost-ui');


module.exports = {
  entry: path.resolve(__dirname, 'src', 'main.js'),
  context: path.join(__dirname, 'src'),
  output: {
    path: path.resolve(__dirname, 'dist_dev'),
    filename: "bundle_" + version + ".js",
    publicPath: '/'
  },
  resolve: {
    extensions: ['.js', '.jsx', '.css', '.scss', '.sass'],
    modules: ['src', path.resolve(__dirname, 'node_modules')]
  },
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        loader: 'babel-loader',
        query: {
          presets: ['es2015', 'stage-1', 'react'],
          plugins: ['transform-decorators-legacy']
        }
      },
      {
        test: /\.css$/,
        use: [
          { loader: 'style-loader'},
          { loader: 'css-loader' }
        ]
      },
      {
        test: /\.scss$|\.sass$/,
        use: [
          { loader: "style-loader" },
          { loader: "css-loader" },
          { loader: "sass-loader" }
        ]
      },
      {
        test: /\.woff2?(\?v=\d+\.\d+\.\d+)?$/,
        loader: 'url-loader',
        options: {
          limit: 10000,
          mimeType: 'application/font-woff'
        }
      },
      {
        test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
        loader: 'url-loader',
        options: {
          limit: 10000,
          mimeType: 'application/octet-stream'
        }
      },
      {
        test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
        loader: 'file-loader'
      },
      {
        test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
        loader: 'url-loader',
        options: {
          limit: 10000,
          mimeType: 'mimetype=image/svg+xml'
        }
      }
    ]
  },
  plugins: [
    new copyPlugin([
      {
        context: lampostSrc,
        from: 'assets/lampost.png'
      },
      {
        context: lampostSrc,
        from: 'assets/raw.css'
      },
      {
        context: lampostSrc,
        from: 'assets/lampost.html',
        transform: function(content) {
          var contentStr = content.toString('utf8');
          var newStr = contentStr.replace(/\$VERSION/g, version);
          return new Buffer(newStr, 'utf8');
        },
        to: 'index.html'
      }

    ])
  ]
};
