#! /usr/bin/env node

const path = require('path');
const webpack = require('webpack');
const cleanPlugin = require('clean-webpack-plugin');
const config = require('./webpack.config.js');

config.output.path = path.resolve(__dirname, 'dist');
config.plugins.push(new cleanPlugin([config.output.path]));
config.plugins.push(new webpack.DefinePlugin({
  'process.env': {
    NODE_ENV: JSON.stringify('production')
  }
}));
config.plugins.push(new webpack.optimize.UglifyJsPlugin());
config.plugins.push(new webpack.optimize.AggressiveMergingPlugin());

webpack(config, (err, stats) => {
  if (err) {
    console.error(err);
    return;
  }

  console.log(stats.toString({
    chunks: false,
    colors: true
  }));
});
